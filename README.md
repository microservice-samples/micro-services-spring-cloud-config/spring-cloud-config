# Spring Cloud Config

### Things todo list:

1. Clone this
   repository: `gir clone https://gitlab.com/microservice-samples/micro-services-spring-cloud-config/spring-cloud-config.git`
2. Navigate to the folder: `cd spring-cloud-config`
3. Run the application: `mvn clean spring-boot:run`
4. Open your favorite browser: htpp://localhost:8888/app-about-company.properties
5. Open your favorite browser: htpp://localhost:8888/spring-cloud-hello-service.properties

Article Link:

* https://o7planning.org/11723/understanding-spring-cloud-config-server-with-example
* https://o7planning.org/11727/understanding-spring-cloud-config-client-with-example